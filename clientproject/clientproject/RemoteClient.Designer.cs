﻿namespace ClientProject
{
    partial class RemoteClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RemoteClient));
            this.RCnotify = new System.Windows.Forms.NotifyIcon(this.components);
            this.newConnLabel = new System.Windows.Forms.Label();
            this.serverAddrText = new System.Windows.Forms.TextBox();
            this.okConnButton = new System.Windows.Forms.Button();
            this.cancelConnButton = new System.Windows.Forms.Button();
            this.modifierText = new System.Windows.Forms.TextBox();
            this.addModButton = new System.Windows.Forms.Button();
            this.sendCharButton = new System.Windows.Forms.Button();
            this.sendGroupBox = new System.Windows.Forms.GroupBox();
            this.charComboBox = new System.Windows.Forms.ComboBox();
            this.addCharButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.clearModButton = new System.Windows.Forms.Button();
            this.modComboBox = new System.Windows.Forms.ComboBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.focus_now = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.iconColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.nameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.percColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.closeConnButton = new System.Windows.Forms.Button();
            this.portLabel = new System.Windows.Forms.Label();
            this.portText = new System.Windows.Forms.TextBox();
            this.portToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.sendGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // RCnotify
            // 
            this.RCnotify.Icon = ((System.Drawing.Icon)(resources.GetObject("RCnotify.Icon")));
            this.RCnotify.Text = "RC";
            this.RCnotify.Visible = true;
            this.RCnotify.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.RCnotify_MouseDoubleClick);
            // 
            // newConnLabel
            // 
            this.newConnLabel.AutoSize = true;
            this.newConnLabel.Location = new System.Drawing.Point(12, 9);
            this.newConnLabel.Name = "newConnLabel";
            this.newConnLabel.Size = new System.Drawing.Size(86, 13);
            this.newConnLabel.TabIndex = 0;
            this.newConnLabel.Text = "New Connection";
            // 
            // serverAddrText
            // 
            this.serverAddrText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serverAddrText.ForeColor = System.Drawing.SystemColors.ControlText;
            this.serverAddrText.Location = new System.Drawing.Point(104, 6);
            this.serverAddrText.Name = "serverAddrText";
            this.serverAddrText.Size = new System.Drawing.Size(121, 20);
            this.serverAddrText.TabIndex = 1;
            this.portToolTip.SetToolTip(this.serverAddrText, "Type the address number");
            this.serverAddrText.MouseMove += new System.Windows.Forms.MouseEventHandler(this.serverAddrText_MouseMove);
            // 
            // okConnButton
            // 
            this.okConnButton.Location = new System.Drawing.Point(367, 4);
            this.okConnButton.Name = "okConnButton";
            this.okConnButton.Size = new System.Drawing.Size(48, 23);
            this.okConnButton.TabIndex = 2;
            this.okConnButton.Text = "OK";
            this.okConnButton.UseVisualStyleBackColor = true;
            this.okConnButton.Click += new System.EventHandler(this.okConnButton_Click);
            // 
            // cancelConnButton
            // 
            this.cancelConnButton.Location = new System.Drawing.Point(430, 4);
            this.cancelConnButton.Name = "cancelConnButton";
            this.cancelConnButton.Size = new System.Drawing.Size(48, 23);
            this.cancelConnButton.TabIndex = 3;
            this.cancelConnButton.Text = "Cancel";
            this.cancelConnButton.UseVisualStyleBackColor = true;
            this.cancelConnButton.Click += new System.EventHandler(this.cancelConnButton_Click);
            // 
            // modifierText
            // 
            this.modifierText.Enabled = false;
            this.modifierText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modifierText.ForeColor = System.Drawing.SystemColors.ControlText;
            this.modifierText.Location = new System.Drawing.Point(22, 152);
            this.modifierText.Name = "modifierText";
            this.modifierText.Size = new System.Drawing.Size(183, 20);
            this.modifierText.TabIndex = 5;
            this.portToolTip.SetToolTip(this.modifierText, "Modifiers");
            // 
            // addModButton
            // 
            this.addModButton.Location = new System.Drawing.Point(218, 67);
            this.addModButton.Name = "addModButton";
            this.addModButton.Size = new System.Drawing.Size(50, 23);
            this.addModButton.TabIndex = 6;
            this.addModButton.Text = "Add";
            this.addModButton.UseVisualStyleBackColor = true;
            this.addModButton.Click += new System.EventHandler(this.addModButton_Click);
            // 
            // sendCharButton
            // 
            this.sendCharButton.Enabled = false;
            this.sendCharButton.Location = new System.Drawing.Point(120, 200);
            this.sendCharButton.Name = "sendCharButton";
            this.sendCharButton.Size = new System.Drawing.Size(75, 23);
            this.sendCharButton.TabIndex = 8;
            this.sendCharButton.Text = "Send";
            this.sendCharButton.UseVisualStyleBackColor = true;
            this.sendCharButton.Click += new System.EventHandler(this.sendCharButton_Click);
            // 
            // sendGroupBox
            // 
            this.sendGroupBox.Controls.Add(this.charComboBox);
            this.sendGroupBox.Controls.Add(this.addCharButton);
            this.sendGroupBox.Controls.Add(this.label1);
            this.sendGroupBox.Controls.Add(this.clearModButton);
            this.sendGroupBox.Controls.Add(this.modComboBox);
            this.sendGroupBox.Controls.Add(this.sendCharButton);
            this.sendGroupBox.Controls.Add(this.addModButton);
            this.sendGroupBox.Controls.Add(this.modifierText);
            this.sendGroupBox.Location = new System.Drawing.Point(600, 49);
            this.sendGroupBox.Name = "sendGroupBox";
            this.sendGroupBox.Size = new System.Drawing.Size(306, 260);
            this.sendGroupBox.TabIndex = 9;
            this.sendGroupBox.TabStop = false;
            // 
            // charComboBox
            // 
            this.charComboBox.FormattingEnabled = true;
            this.charComboBox.Items.AddRange(new object[] {
            "a",
            "b",
            "c",
            "d",
            "e",
            "f",
            "g",
            "h",
            "i",
            "j",
            "k",
            "l",
            "m",
            "n",
            "o",
            "p",
            "q",
            "r",
            "s",
            "t",
            "u",
            "v",
            "w",
            "x",
            "y",
            "z",
            " ",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "0"});
            this.charComboBox.Location = new System.Drawing.Point(22, 112);
            this.charComboBox.Name = "charComboBox";
            this.charComboBox.Size = new System.Drawing.Size(183, 21);
            this.charComboBox.TabIndex = 13;
            // 
            // addCharButton
            // 
            this.addCharButton.Location = new System.Drawing.Point(218, 108);
            this.addCharButton.Name = "addCharButton";
            this.addCharButton.Size = new System.Drawing.Size(50, 23);
            this.addCharButton.TabIndex = 12;
            this.addCharButton.Text = "Add";
            this.addCharButton.UseVisualStyleBackColor = true;
            this.addCharButton.Click += new System.EventHandler(this.addCharButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(110, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Send to focus app";
            // 
            // clearModButton
            // 
            this.clearModButton.Location = new System.Drawing.Point(218, 149);
            this.clearModButton.Name = "clearModButton";
            this.clearModButton.Size = new System.Drawing.Size(50, 23);
            this.clearModButton.TabIndex = 10;
            this.clearModButton.Text = "Clear";
            this.clearModButton.UseVisualStyleBackColor = true;
            this.clearModButton.Click += new System.EventHandler(this.clearModButton_Click);
            // 
            // modComboBox
            // 
            this.modComboBox.FormattingEnabled = true;
            this.modComboBox.Items.AddRange(new object[] {
            "BACKSPACE",
            "TAB",
            "CLEAR",
            "ENTER",
            "SHIFT",
            "CTRL",
            "ALT",
            "PAUSE",
            "CAPS LOCK",
            "ESC",
            "SPACEBAR",
            "PAGE UP",
            "PAGE DOWN",
            "END",
            "HOME",
            "LEFT ARROW",
            "UP ARROW",
            "RIGHT ARROW",
            "DOWN ARROW",
            "SELECT",
            "PRINT",
            "EXECUTE",
            "PRINT SCREEN",
            "INS",
            "DEL",
            "HELP",
            "LEFT WINDOW KEY",
            "RIGHT WINDOW KEY",
            "APPLICATIONS KEY",
            "COMPUTER SLEEP",
            "NUMERIC PAD 0",
            "NUMERIC PAD 1",
            "NUMERIC PAD 2",
            "NUMERIC PAD 3",
            "NUMERIC PAD 4",
            "NUMERIC PAD 5",
            "NUMERIC PAD 6",
            "NUMERIC PAD 7",
            "NUMERIC PAD 8",
            "NUMERIC PAD 9",
            "MULTIPLY",
            "ADD",
            "SEPARATOR",
            "SUBTRACT",
            "DECIMAL",
            "DIVIDE",
            "F1",
            "F2",
            "F3",
            "F4",
            "F5",
            "F6",
            "F7",
            "F8",
            "F9",
            "F10",
            "F11",
            "F12",
            "F13",
            "F14",
            "F15",
            "F16",
            "F17",
            "F18",
            "F19",
            "F20",
            "F21",
            "F22",
            "F23",
            "F24",
            "NUM LOCK",
            "SCROLL LOCK",
            "LEFT SHIFT",
            "RIGHT SHIFT",
            "LEFT CONTROL",
            "RIGHT CONTROL",
            "LEFT MENU",
            "RIGHT MENU",
            "ZOOM"});
            this.modComboBox.Location = new System.Drawing.Point(22, 70);
            this.modComboBox.Name = "modComboBox";
            this.modComboBox.Size = new System.Drawing.Size(183, 21);
            this.modComboBox.TabIndex = 9;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.focus_now,
            this.iconColumn,
            this.nameColumn,
            this.percColumn});
            this.dataGridView1.Location = new System.Drawing.Point(15, 85);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(463, 298);
            this.dataGridView1.TabIndex = 11;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // focus_now
            // 
            this.focus_now.HeaderText = "";
            this.focus_now.Name = "focus_now";
            this.focus_now.ReadOnly = true;
            this.focus_now.Width = 20;
            // 
            // iconColumn
            // 
            this.iconColumn.HeaderText = "Icon";
            this.iconColumn.Name = "iconColumn";
            this.iconColumn.ReadOnly = true;
            this.iconColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.iconColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // nameColumn
            // 
            this.nameColumn.FillWeight = 150F;
            this.nameColumn.HeaderText = "Name";
            this.nameColumn.Name = "nameColumn";
            this.nameColumn.ReadOnly = true;
            this.nameColumn.Width = 200;
            // 
            // percColumn
            // 
            this.percColumn.HeaderText = "% Focus";
            this.percColumn.Name = "percColumn";
            this.percColumn.ReadOnly = true;
            // 
            // closeConnButton
            // 
            this.closeConnButton.Enabled = false;
            this.closeConnButton.Location = new System.Drawing.Point(240, 47);
            this.closeConnButton.Name = "closeConnButton";
            this.closeConnButton.Size = new System.Drawing.Size(111, 23);
            this.closeConnButton.TabIndex = 12;
            this.closeConnButton.Text = "Close Connection";
            this.closeConnButton.UseVisualStyleBackColor = true;
            this.closeConnButton.Click += new System.EventHandler(this.closeConnButton_Click);
            // 
            // portLabel
            // 
            this.portLabel.AutoSize = true;
            this.portLabel.Location = new System.Drawing.Point(246, 9);
            this.portLabel.Name = "portLabel";
            this.portLabel.Size = new System.Drawing.Size(26, 13);
            this.portLabel.TabIndex = 13;
            this.portLabel.Text = "Port";
            // 
            // portText
            // 
            this.portText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.portText.ForeColor = System.Drawing.SystemColors.ControlText;
            this.portText.Location = new System.Drawing.Point(278, 6);
            this.portText.Name = "portText";
            this.portText.Size = new System.Drawing.Size(73, 20);
            this.portText.TabIndex = 14;
            this.portToolTip.SetToolTip(this.portText, "Type the port number");
            // 
            // portToolTip
            // 
            this.portToolTip.AutoPopDelay = 5000;
            this.portToolTip.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.portToolTip.InitialDelay = 100;
            this.portToolTip.ReshowDelay = 100;
            // 
            // RemoteClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(937, 389);
            this.Controls.Add(this.portText);
            this.Controls.Add(this.portLabel);
            this.Controls.Add(this.closeConnButton);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.sendGroupBox);
            this.Controls.Add(this.cancelConnButton);
            this.Controls.Add(this.okConnButton);
            this.Controls.Add(this.serverAddrText);
            this.Controls.Add(this.newConnLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RemoteClient";
            this.Text = "Remote Client";
            this.Load += new System.EventHandler(this.RemoteClient_Load);
            this.sendGroupBox.ResumeLayout(false);
            this.sendGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NotifyIcon RCnotify;
        private System.Windows.Forms.Label newConnLabel;
        public System.Windows.Forms.TextBox serverAddrText;
        private System.Windows.Forms.Button okConnButton;
        private System.Windows.Forms.Button cancelConnButton;
        private System.Windows.Forms.TextBox modifierText;
        private System.Windows.Forms.Button addModButton;
        private System.Windows.Forms.Button sendCharButton;
        private System.Windows.Forms.GroupBox sendGroupBox;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox modComboBox;
        private System.Windows.Forms.Button clearModButton;
        private System.Windows.Forms.Button closeConnButton;
        private System.Windows.Forms.Label portLabel;
        public System.Windows.Forms.TextBox portText;
        private System.Windows.Forms.ToolTip portToolTip;
        private System.Windows.Forms.DataGridViewCheckBoxColumn focus_now;
        private System.Windows.Forms.DataGridViewImageColumn iconColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn percColumn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox charComboBox;
        private System.Windows.Forms.Button addCharButton;
    }






}

