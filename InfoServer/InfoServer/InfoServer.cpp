// server_project.cpp : definisce il punto di ingresso dell'applicazione console.
//
//#pragma once


#undef UNICODE

//#define new NEW_DEBUG
//#define _CRT_SECURE_NO_WARNINGS
#define WIN32_LEAN_AND_MEAN
#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include "stdafx.h"
#include <windows.h>
#include <regex>
//#include <RdpEncomAPI.h>
#include <Windows.h>
#include <atlbase.h>
//#include <winsock2.h>
#include <ws2tcpip.h>
#include <WinUser.h>
#include <iostream>
#include <fstream>
#include <shellapi.h>
#include <map>
#include <thread>
#include <stdlib.h>
#include <stdio.h>
#include <set>
#include <shellapi.h>
#include <Psapi.h>
#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <olectl.h>
#include <map>
#include <json.hpp>
#pragma comment(lib, "oleaut32.lib")
#include "JsonWindow.h"
//#include "JsonFocus.h"
//#include "KeyboardKey.h"

using json = nlohmann::json;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")
// #pragma comment (lib, "Mswsock.lib")



std::map<WORD, char*> KeyCodes = { { VK_BACK, "BACKSPACE" },
{ VK_TAB, "TAB" },
{ VK_CLEAR, "CLEAR" },
{ VK_RETURN, "ENTER" },
{ VK_SHIFT, "SHIFT" },
{ VK_CONTROL, "CTRL" },
{ VK_MENU, "ALT" },
{ VK_PAUSE, "PAUSE" },
{ VK_CAPITAL, "CAPS LOCK" },
{ VK_ESCAPE, "ESC" },
{ VK_SPACE, "SPACEBAR" },
{ VK_PRIOR, "PAGE UP" },
{ VK_NEXT, "PAGE DOWN" },
{ VK_END, "END" },
{ VK_HOME, "HOME" },
{ VK_LEFT, "LEFT ARROW" },
{ VK_UP, "UP ARROW" },
{ VK_RIGHT, "RIGHT ARROW" },
{ VK_DOWN, "DOWN ARROW" },
{ VK_SELECT, "SELECT" },
{ VK_PRINT, "PRINT" },
{ VK_EXECUTE, "EXECUTE" },
{ VK_SNAPSHOT, "PRINT SCREEN" },
{ VK_INSERT, "INS" },
{ VK_DELETE, "DEL" },
{ VK_HELP, "HELP" },
{ VK_LWIN, "LEFT WINDOWS KEY" },
{ VK_RWIN, "RIGHT WINDOWS KEY" },
{ VK_APPS, "APPLICATIONS KEY" },
{ VK_SLEEP, "COMPUTER SLEEP" },
{ VK_NUMPAD0, "NUMERIC KEYPAD 0" },
{ VK_NUMPAD1, "NUMERIC KEYPAD 1" },
{ VK_NUMPAD2, "NUMERIC KEYPAD 2" },
{ VK_NUMPAD3, "NUMERIC KEYPAD 3" },
{ VK_NUMPAD4, "NUMERIC KEYPAD 4" },
{ VK_NUMPAD5, "NUMERIC KEYPAD 5" },
{ VK_NUMPAD6, "NUMERIC KEYPAD 6" },
{ VK_NUMPAD7, "NUMERIC KEYPAD 7" },
{ VK_NUMPAD8, "NUMERIC KEYPAD 8" },
{ VK_NUMPAD9, "NUMERIC KEYPAD 9" },
{ VK_MULTIPLY, "MULTIPLY" },
{ VK_ADD, "ADD" },
{ VK_SEPARATOR, "SEPARATOR" },
{ VK_SUBTRACT, "SUBTRACT" },
{ VK_DECIMAL, "DECIMAL" },
{ VK_DIVIDE, "DIVIDE" },
{ VK_F1, "F1" },
{ VK_F2, "F2" },
{ VK_F3, "F3" },
{ VK_F4, "F4" },
{ VK_F5, "F5" },
{ VK_F6, "F6" },
{ VK_F7, "F7" },
{ VK_F8, "F8" },
{ VK_F9, "F9" },
{ VK_F10, "F10" },
{ VK_F11, "F11" },
{ VK_F12, "F12" },
{ VK_F13, "F13" },
{ VK_F14, "F14" },
{ VK_F15, "F15" },
{ VK_F16, "F16" },
{ VK_F17, "F17" },
{ VK_F18, "F18" },
{ VK_F19, "F19" },
{ VK_F20, "F20" },
{ VK_F21, "F21" },
{ VK_F22, "F22" },
{ VK_F23, "F23" },
{ VK_NUMLOCK, "NUM LOCK" },
{ VK_SCROLL, "SCROLL LOCK" },
{ VK_LSHIFT, "LEFT SHIFT" },
{ VK_RSHIFT, "RIGHT SHIFT" },
{ VK_LCONTROL, "LEFT CONTROL" },
{ VK_RCONTROL, "RIGHT CONTROL" },
{ VK_LMENU, "LEFT MENU" },
{ VK_RMENU, "RIGHT MENU" },
{ VK_ZOOM, "ZOOM" }

};

//*****************************
BOOL CALLBACK EnumWindowsProc(HWND hwnd, LPARAM lParam);
BOOL IsAltTabWindow(HWND hwnd);	//per controllare se la window è da considerare
WORD ConvertToWord(char* code);//per convertire un tasto nel codice corrispondente
DWORD WINAPI ThreadSendInput();//funzione del thread che ascolta i tasti
HRESULT SaveIcon(HICON hIcon, LPCSTR path);//funzione per tradurre un'icona in un file
int getFileSize(const std::string &fileName);
//*****************************


const int DEFAULT_BUFLEN = 512;
PCSTR DEFAULT_PORT = "300";
const int MAX_FILE = 1000;


//int enumcounter = 0;
DWORD focusPID, oldFocus = NULL;	
SOCKET ClientSocket = INVALID_SOCKET;
std::set<string> pid_mandati;
std::map<string, int> timesPids = map<string, int>();

bool sendFlag = true;
bool no_connection = FALSE;
int countSleep = 0;

//using namespace System;
using namespace std;

string actual_focus = "0";


int main()
{
	
	SetConsoleTitle(TEXT("InfoServer"));
	printf("InfoServer - Benvenuto!\n");

	WSADATA wsaData;
	int iResult;

	SOCKET ListenSocket = INVALID_SOCKET;


	struct addrinfo *result = NULL;
	struct addrinfo hints;

	int iSendResult;
	char recvbuf[DEFAULT_BUFLEN];
	int recvbuflen = DEFAULT_BUFLEN;

	
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	
	ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (ListenSocket == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		return 1;
	}

	iResult = 0;
	
	::bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	freeaddrinfo(result);


	iResult = listen(ListenSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}


	while (1) {
		printf("dentro while\n");
		struct sockaddr_in sockddr;
		socklen_t len;
		ClientSocket = accept(ListenSocket, (struct sockaddr*)&sockddr, sizeof(sockddr) < 0);
		if (ClientSocket == INVALID_SOCKET) {
			printf("accept failed with error: %d\n", WSAGetLastError());
			closesocket(ListenSocket);
			WSACleanup();
			//return 1;
			continue;
		}
		else {
			//u_short number = ntohs(sockddr.sin_addr);
			int lung = sizeof(sockddr);
			getsockname(ListenSocket, (struct sockaddr*)&sockddr, &lung);
			char *connected_ip = inet_ntoa(sockddr.sin_addr);
		//	printf("Connessione a %s\n", connected_ip);
			
		}
		int timeout = 10000000;//millisecondi
		setsockopt(ClientSocket, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(int));
		int timeoutFlag = 0;

		//Handshake
		




		//**************************************************************************************
		//connected


		DWORD   dwThreadIdArray[1];
		HANDLE  hThreadArray[DEFAULT_BUFLEN];
		


		
			hThreadArray[0] = CreateThread(		//thread per ricevere le combinazioni di tasti da inviare
				NULL,                   // default security attributes
				0,                      // use default stack size  
				(LPTHREAD_START_ROUTINE)ThreadSendInput,       // thread function name
				NULL, //(LPVOID*)ClientSocket,          // argument to thread function 
				0,                      // use default creation flags 
				&dwThreadIdArray[0]);


			
			sendFlag = true;
			int totTimes=0;

			countSleep = 0;
			int numerovolte = 1;
			while (1) {
				//printf("dentro secondo whiòe, %d\n", numerovolte++);
				
				if (no_connection || countSleep > 40000000000) {
					no_connection = false;
					break;
				}
				HWND selected = GetForegroundWindow();




				countSleep++;




				//cout << "prende focus" << endl;
				GetWindowThreadProcessId(selected, &focusPID);	//prendo PID del processo in focus
				totTimes++;
				string string_pid = std::to_string(focusPID);
				if (string_pid.compare(actual_focus) != 0) {
					sendFlag = true;
					actual_focus = string_pid;
				}
				//cout << "focus: " << string_pid << endl;

				//carico stringpid in timespids
				//if (timesPids.find(string_pid)!= timesPids.end) {
				if (timesPids.count(string_pid)) {
					timesPids[string_pid] ++;
				}
				else {
					timesPids[string_pid] = 1;
				}

				if (sendFlag == false) {
					continue;
				}

				//manda "SND"
				iSendResult = send(ClientSocket, "SND", 3, 0);//invio stringa delle info, con 0 finale//o 16
				if (iSendResult == SOCKET_ERROR) {
					printf("send failed with error: %d\n", WSAGetLastError());
					closesocket(ClientSocket);
					//WSACleanup();
					//return 1;
					no_connection = true;
					break;
				}
				//else
					//printf("sent:\n%s\n", "SND");

				countSleep = 0;
				cout << "enum" << endl;
				EnumWindows(EnumWindowsProc, NULL);
				//cout << "dopo enumwindowsproc" << endl;
				pid_mandati.clear();
				//Sleep(30000);

				
				bool connection_closed = FALSE;


				if (connection_closed) {
					no_connection = TRUE;
					break;
				}
				
				

				JsonWindow focusJson = JsonWindow(2, " ", string_pid, false, " ", 0);
				/*if (focusJson.saveToFile()) {

					cout << "non è riuscito a mandare il focus\n" << endl;
					return 1;
				}*/
				//focusJson.print();

				string daMandare = focusJson.saveToString();

				//std::ifstream ifs;

				
				//ifs.open("testWindow.txt", std::ifstream::in);
				
				int fileSize = daMandare.length();
				char file_send[1000];// = "SIZE = " + fileSize;
				//strcpy(file_send, daMandare.c_str());
				sprintf(file_send, "SIZE = %d", fileSize);
				//strcpy(file_send, "SIZE = " + fileSize);
				//std::string file_send = "SIZE = " + fileSize;
				iSendResult = send(ClientSocket, file_send, 12, 0);//invio stringa delle info, con 0 finale//o 16
				if (iSendResult == SOCKET_ERROR) {
					printf("send failed with error: %d\n", WSAGetLastError());
					closesocket(ClientSocket);
					//WSACleanup();
					//return 1;
					no_connection = true;
					break;
				}
				//else
					//printf("sent: %d bytes\n%s\n", iSendResult, file_send);


				//ifs.getline(file_send, fileSize + 1);
				//remove("testWindow.txt");
				strcpy(file_send, daMandare.c_str());
				file_send[fileSize] = '\0';
				iSendResult = send(ClientSocket, file_send, fileSize + 1, 0);//invio stringa delle info, con 0 finale//o 16
				if (iSendResult == SOCKET_ERROR) {
					printf("send failed with error: %d\n", WSAGetLastError());
					closesocket(ClientSocket);
					//WSACleanup();
					//return 1;
					no_connection = true;
					break;
				}
				//else
					//printf("sent:%d bytes\n%s\n", iSendResult, file_send);


				//mappa dei pid con tempi
				JsonWindow mapJson = JsonWindow(3, timesPids, totTimes);
				daMandare = mapJson.saveToString();
				/*if (mapJson.saveToFile()) {
					cout << "non ha salvato le mappe!!!!" << endl;
					return 1;
				}
				//mapJson.print();
				ifs.close();*/

				char file_pend[300] = { '\0' };
				//ifs.open("testWindow.txt", std::ifstream::in);
				fileSize = daMandare.length();
				sprintf(file_pend, "SIZE = %d\0", fileSize);
				//strcpy(file_send, "SIZE = " + fileSize);
				//std::string file_send = "SIZE = " + fileSize;
				iSendResult = send(ClientSocket, file_pend, 12, 0);//invio stringa delle info, con 0 finale//o 16
				if (iSendResult == SOCKET_ERROR) {
					printf("send failed with error: %d\n", WSAGetLastError());
					closesocket(ClientSocket);
					//WSACleanup();
					//return 1;
					no_connection = true;
					break;
				}
				//else
					//printf("sent:%d bytes\n%s\n", iSendResult, file_pend);


				//ifs.getline(file_pend, fileSize + 1);
				//remove("testWindow.txt");
				strcpy(file_pend, daMandare.c_str());
				file_pend[fileSize] = '\0';
				iSendResult = send(ClientSocket, file_pend, fileSize + 1, 0);//invio stringa delle info, con 0 finale//o 16
				if (iSendResult == SOCKET_ERROR) {
					printf("send failed with error: %d\n", WSAGetLastError());
					closesocket(ClientSocket);
					//WSACleanup();
					//return 1;
					no_connection = true;
					break;
				}
				//else
					//printf("sent:%d bytes\n%s\n", iSendResult, file_pend);





				JsonWindow endJson = JsonWindow(0, " ", string_pid, false, " ", 0);
				/*if (endJson.saveToFile()) {
					cout << "non ha mandato end!" << endl;
					return 1;
				}
				//endJson.print();
				ifs.close();*/
				//std::ifstream ifs;
				daMandare = endJson.saveToString();

				char file_end[1000] = { '\0' };
				//ifs.open("testWindow.txt", std::ifstream::in);
				fileSize = daMandare.length();
				sprintf(file_end, "SIZE = %d\0", fileSize);
				//strcpy(file_send, "SIZE = " + fileSize);
				//std::string file_send = "SIZE = " + fileSize;
				iSendResult = send(ClientSocket, file_end, 12, 0);//invio stringa delle info, con 0 finale//o 16
				if (iSendResult == SOCKET_ERROR) {
					printf("send failed with error: %d\n", WSAGetLastError());
					closesocket(ClientSocket);
					//WSACleanup();
					//return 1;
					no_connection = true;
					break;
				}
				//else
				//	printf("sent:%d bytes\n%s\n", iSendResult, file_end);


				//ifs.getline(file_end, fileSize + 1);
				//remove("testWindow.txt");
				strcpy(file_end, daMandare.c_str());
				file_end[fileSize] = '\0';
				iSendResult = send(ClientSocket, file_end, fileSize + 1, 0);//invio stringa delle info, con 0 finale//o 16
				if (iSendResult == SOCKET_ERROR) {
					printf("send failed with error: %d\n", WSAGetLastError());
					closesocket(ClientSocket);
					//WSACleanup();
					//return 1;
					no_connection = true;
					break;
				}
				//else
				//	printf("sent:%d bytes \n%s\n", iSendResult, file_end);
				sendFlag = false;
				

				//Sleep(1000);//da togliere
			}
			timesPids.clear();
			totTimes = 0;
			TerminateThread(hThreadArray[0], 0);
			CloseHandle(hThreadArray[0]);

			closesocket(ClientSocket);
			
			
			cout << "Connection closed!" << endl;
		
	}


	//**************************************************************************************

	closesocket(ListenSocket);



	return 0;
}

BOOL CALLBACK EnumWindowsProc(HWND hwnd, LPARAM lParam)
{
	
		//printf("dentro en umwindows\n");
		if (!IsWindowEnabled(hwnd))
			return TRUE;
		//if (!IsWindowVisible(hwnd))    
		//	return TRUE;
		if (!IsAltTabWindow(hwnd))
			return TRUE;


		DWORD processID = 0;

		GetWindowThreadProcessId(hwnd, &processID);	//PID del processo della finestra

		string stringaProcesso = std::to_string(processID);

		if (pid_mandati.find(stringaProcesso) != pid_mandati.end()) {//è contenuto
			return TRUE;
		}
		pid_mandati.insert(stringaProcesso);

		LPSTR title = "";

		HANDLE p_handle = OpenProcess(PROCESS_ALL_ACCESS, TRUE, processID);	//handle del processo
																			//nome del file eseguibile

		DWORD name_ex_len, name_for_icons_len;
		//char name_for_icons[DEFAULT_BUFLEN + 1];
		//LPWSTR name_for_icons = nullptr;
		char name_for_icons[DEFAULT_BUFLEN];
		//LPWSTR doppiaw = CA2W(name_for_icons);

		name_for_icons_len = GetModuleFileNameEx(p_handle, NULL, name_for_icons, DEFAULT_BUFLEN);	//codice
		string name_for_icons_string = CT2A(name_for_icons);
		//typedef char* PSTR, *LPSTR;																							//GetProcessImageFileName
		LPWSTR nameProc[DEFAULT_BUFLEN];
		TCHAR charo[DEFAULT_BUFLEN];
		BYTE bytes[DEFAULT_BUFLEN];
		memcpy(bytes, charo, DEFAULT_BUFLEN);

		name_ex_len = GetProcessImageFileName(p_handle, charo, DEFAULT_BUFLEN);//percorso file

		string console = CT2A(charo);

		//string console = string(CW2A(*nameProc));

		//char name_icons_image[DEFAULT_BUFLEN];
		//strcpy(name_icons_image, CW2A(*nameProc));
		//wcstombs(name_icons_image, *nameProc, DEFAULT_BUFLEN);
		string name_with_C = "C:";
		//string nameProc_string(console);

		if (size_t position = console.find("HarddiskVolume")) {

			position += 15;
			name_with_C.append(console, position, DEFAULT_BUFLEN);
		}
		else {
			name_with_C = string(console);
		}

		char *newName = strtok(const_cast<char*>(console.c_str()), "\\");
		//cout << "C O N S O L E &&&&&&&&&&&&&&&&&&&& " << console << endl;
//		cout << "P I D " << stringaProcesso << endl;

		string console2 = "";
		while (newName != NULL) {

			strcpy(const_cast<char*>(console.c_str()), newName);
			console2 = string(newName);
			newName = strtok(NULL, "\\");

		}

		int iSendResult;

		SHFILEINFO FileInfo = { 0 };
		//name_for_icons = name_with_C.c_str();
		//strcpy(CW2A(name_for_icons), name_with_C.c_str());

		//LPCSTR costante = name_for_icons;
		//SHGetFileInfo()
		//LPTSTR tstr;
		//wcstombs(tstr, name_for_icons, sizeof(name_for_icons));
		//std::wstring ws = wstring(name_for_icons);
		//LPTSTR ws = CW2T(name_for_icons);
		SHGetFileInfo(name_for_icons,
			0,
			&FileInfo,
			sizeof(FileInfo),
			SHGFI_ICON | SHGFI_LARGEICON | SHGFI_DISPLAYNAME);

		char icon_buffer_file[2048];

		string iconString = "";

		bool hasIcon = false;
		int fileSizeIco = 0;
		if (FileInfo.hIcon != NULL) {
			hasIcon = true;

			HICON icon = FileInfo.hIcon; // salvare in un file
			char info_icon[DEFAULT_BUFLEN] = {};
			ICONINFO iconinfo;
			//GetIconInfo(icon, &iconinfo);
			//HBITMAP hBitmap = iconinfo.hbmColor;
			//Sleep(10);
			//info_process.append(hBitmap);
			//LPCSTR pathIcon = L"C:\\img.ico"; //
			HRESULT hr = SaveIcon(icon, "img.ico");
			//cout << "HRESULT ====================================>  " << hr << endl;
			ifstream infile;
			fileSizeIco = getFileSize("img.ico");
			
			infile.open("img.ico", ios::binary | ios::in);
			//cout << "infile open icona " <<endl;

			infile.getline(icon_buffer_file, fileSizeIco);
		
			//cout << "ICONAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" << endl;
			//cout << icon_buffer_file << endl;
			//cout << "infile getline " << endl;

			icon_buffer_file[fileSizeIco] = '\0';
			//cout << "infile separatore" << endl;
			infile.close();
//			cout << "infile chiuso " << endl;

			/*if (remove("img.ico") != 0) {
				printf("OH MERDA NON HA TOLTO L'ICONA!!!!!!!!!!!!!!!!!!!\n");
			}
			else {

				printf("OH OK\n");
			}*/
			DestroyIcon(icon);
		}

		JsonWindow window = JsonWindow(1, console2, stringaProcesso, false, " ", 0);
		//cout << "window creato" << endl;

		if (hasIcon) {

			window = JsonWindow(1, console2, stringaProcesso, true, "i", fileSizeIco);
			//cout << "window ha icona" << endl;

		}

		remove("testWindow.txt");
		//cout << "test rimosso" << endl;

		//window.print();
		//cout << "prova a salvare json" << endl;
		/*try {
			if (window.saveToFile()) {
				cout << "salvataggio andato a male" << endl;
				return 1;
			}
			//window.print();
		}
		catch (const std::exception& e) {
			DWORD dw = GetLastError();
			printf("-----------------------------------------eccezione, ultimo errore %u\n", dw);
			cout << errno << endl;
			printf("json: %s \n", window.getName());
			//printf("nome vero: %s\n", console2);
			//if (dw != ERROR_SUCCESS) {
				return 1;
			//}
		}*/
		//cout << "json salvato" << endl;
		string daMandare = window.saveToString();
		//std::ifstream ifs;

		//ifs.open("testWindow.txt", std::ifstream::in);
		int fileSize = daMandare.length();

		//cout << "la dimensione è ??????????????????????????????????? " << fileSize << endl;
		if (fileSize <= 0) {
			return TRUE;
		}

		char file_send[13];
		sprintf(file_send, "SIZE = %d", fileSize);

		try {
			iSendResult = send(ClientSocket, file_send, 12, 0);
			if (iSendResult == SOCKET_ERROR) {
				printf("send failed with error: %d\n", WSAGetLastError());
				closesocket(ClientSocket);
				//WSACleanup();
				return 1;
			}
		//	else 
			//	printf("sent:%d bytes\n%s\n", iSendResult, file_send);
		}
		catch (const std::exception& e) {
			printf("eccezione, non manda size \n");
		}

		int bytes_sent = 0;

		char *fileAll = (char*)malloc(fileSize + 1 * sizeof(char));
		//ifs.getline(fileAll, fileSize + 1);
		strcpy(fileAll, daMandare.c_str());
		//cout << "fileall getline" << endl;
		//remove("testWindow.txt");
		//cout << "test rimosso" << endl;

		string fileString = string(fileAll);//7
		//cout << "fileall fatta stringa" << endl;
		try {
			iSendResult = send(ClientSocket, fileAll, fileSize + 1, 0);//invio stringa delle info, con 0 finale//o 16
			if (iSendResult == SOCKET_ERROR) {
				printf("send failed with error: %d\n", WSAGetLastError());
				closesocket(ClientSocket);
				//WSACleanup();
				return 1;
			}
			//else
			//	printf("megacacca", iSendResult, fileAll);
		}
		catch (const std::exception& e) {
			DWORD dw = GetLastError();
			printf("eccezione, non manda il json %u\n", dw);
		}

		if (hasIcon) {
			for (int indice = 0; indice < fileSizeIco; indice++) {
				//const unsigned char uc = (unsigned char)icon_buffer_file[indice];
				try {
					iSendResult = send(ClientSocket, &icon_buffer_file[indice], 1, 0);//invio stringa delle info, con 0 finale//o 16
					if (iSendResult == SOCKET_ERROR) {
						printf("send failed with error: %d\n", WSAGetLastError());
						closesocket(ClientSocket);
						//WSACleanup();
						return 1;
					}
				}
				catch (const std::exception& e) {
					printf("eccezione, non manda icona \n");
				}
			}
		}
	

	return TRUE;
}


BOOL IsAltTabWindow(HWND hwnd)
{
	TITLEBARINFO ti;
	HWND hwndTry, hwndWalk = NULL;

	if (!IsWindowVisible(hwnd))
		return FALSE;

	hwndTry = GetAncestor(hwnd, GA_ROOTOWNER);
	while (hwndTry != hwndWalk)
	{
		hwndWalk = hwndTry;
		hwndTry = GetLastActivePopup(hwndWalk);
		if (IsWindowVisible(hwndTry))
			break;
	}
	if (hwndWalk != hwnd)
		return FALSE;


	ti.cbSize = sizeof(ti);
	GetTitleBarInfo(hwnd, &ti);
	if (ti.rgstate[0] & STATE_SYSTEM_INVISIBLE)
		return FALSE;

	
	if (GetWindowLong(hwnd, GWL_EXSTYLE) & WS_EX_TOOLWINDOW)
		return FALSE;

	return TRUE;
}

DWORD WINAPI ThreadSendInput() {

	printf("Wait for client commands...\n");

	//Sleep(5000);
	int iResult;
	char recvbuf[DEFAULT_BUFLEN];
	//char recvbufnopid[DEFAULT_BUFLEN];
	int recvbuflen = DEFAULT_BUFLEN;


	while (1) {

		iResult = recv(ClientSocket, recvbuf, recvbuflen, 0);
		countSleep = 0;
		if (iResult > 0) {
			printf("Bytes received from client: %d\n", iResult);
			//string recvStr = string(recvbuf);
			if (strstr(recvbuf, "SENDDATA") != nullptr) {
				//si chiede di mandare dati
				sendFlag = true;
			}
			else if (strstr(recvbuf, "CLOSECONNECTION") != nullptr) {
				//chiusura connessione
				no_connection = TRUE;
			}
			else {
				printf("Command received!\n");
				HWND selected_thread = GetForegroundWindow();
				DWORD focus_win;
				GetWindowThreadProcessId(selected_thread, &focus_win);

				SetFocus(selected_thread);

				INPUT inputs[10];
				int count = 0;

				//mettere controllo PID

				//keyup
				char *command = strtok(recvbuf, ";");//ritorna il pid del focus

				command = strtok(NULL, ";");//prendere un comando alla volta
				WORD words[10];
				while (command != NULL && strstr(command, "ENDMSG")== NULL) {

					//printf("command: %s", command);

					INPUT ip;
					ip.type = INPUT_KEYBOARD;
					ip.ki.wScan = 0; // hardware scan code for key
					ip.ki.time = 0;
					ip.ki.dwExtraInfo = 0;

					//controllare anche nel caso di caratteri!

					WORD vKey = ConvertToWord(command);
					words[count] = vKey;					
					if (vKey != NULL) {						
						ip.ki.wVk = vKey; // virtual-key code for the "a" key
						ip.ki.dwFlags = 0; // 0 for key press										
							//SendInput(1, &ip, sizeof(INPUT));
						inputs[count] = ip;
						count++;
						command = strtok(NULL, ";");
					}

				}

				//keydown - al contrario!
				char *command2 = strtok(recvbuf, ";");//ritorna il pid del focus

				int w = count;
				//command2 = strtok(NULL, ";");//prendere un comando alla volta
				//while (command2 != NULL && strstr(command2, "ENDMSG") == NULL) {
				while (w > 0) {
					w--;
					//printf("command: %s", command);

					INPUT ip;
					ip.type = INPUT_KEYBOARD;
					ip.ki.wScan = 0; // hardware scan code for key
					ip.ki.time = 0;
					ip.ki.dwExtraInfo = 0;

					//controllare anche nel caso di caratteri!

					WORD vKey = words[w];
					//if (vKey != NULL) {
					ip.ki.wVk = vKey; // virtual-key code for the "a" key
					ip.ki.dwFlags = KEYEVENTF_KEYUP; // 0 for key press										
									   //SendInput(1, &ip, sizeof(INPUT));
					inputs[count] = ip;
					count++;
					//command2 = strtok(NULL, ";");
				}
			

				
				SendInput(count, inputs, sizeof(INPUT));
			}
		}



	}

}

WORD ConvertToWord(char* code) {
	char temp[DEFAULT_BUFLEN];
	strcpy(temp, code);
	if (isalnum(temp[0]) && temp[1] == '\0') {
		if (isdigit(temp[0])) {
			return temp[0];
		}
		else {
			return toupper(temp[0]);
		}
	}

	map <WORD, char*>::iterator it;
	//map<WORD, char*> temp = KeyboardKey:: getMap();
	//for (it = temp.begin(); it != temp.end(); it++) {
	for (it = KeyCodes.begin(); it != KeyCodes.end(); it++) {
		if (strcmp(it->second, temp)==0) {
			return it->first;
		}
	}
	return NULL;
}

HRESULT SaveIcon(HICON hIcon, LPCSTR path) {

	//cout << "dentro saveicon::::::::::::::::::::::::::::::::::::::::::: " << path << endl;
	PICTDESC desc = { sizeof(PICTDESC) };
	desc.picType = PICTYPE_ICON;
	desc.icon.hicon = hIcon;
	IPicture* pPicture = 0;
	HRESULT hr = OleCreatePictureIndirect(&desc, IID_IPicture, FALSE, (void**)&pPicture);
	if (FAILED(hr)) return hr;


	IStream* pStream = 0;
	CreateStreamOnHGlobal(0, TRUE, &pStream);
	LONG cbSize = 0;
	hr = pPicture->SaveAsFile(pStream, TRUE, &cbSize);


	if (!FAILED(hr)) {
		HGLOBAL hBuf = 0;
		GetHGlobalFromStream(pStream, &hBuf);
		void* buffer = GlobalLock(hBuf);
		HANDLE hFile = CreateFile(path, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, 0, 0);

		if (!hFile) hr = HRESULT_FROM_WIN32(GetLastError());
		else {
			DWORD written = 0;
			WriteFile(hFile, buffer, cbSize, &written, 0);
			CloseHandle(hFile);
		}
		GlobalUnlock(buffer);
	}

	pStream->Release();
	pPicture->Release();

	return hr;
}


int getFileSize(const std::string &fileName)
{
	ifstream file(fileName.c_str(), ifstream::in | ifstream::binary);

	if (!file.is_open())
	{
		return -1;
	}

	file.seekg(0, ios::end);
	int fileSize = file.tellg();
	file.close();

	return fileSize;
}